import java.util.HashSet;
import java.util.List;
import java.util.Set;


public aspect MoveTracking {
	private static boolean flag = false;
	private static Set movees = new HashSet();
		
	public static boolean testAndClear() {	
		boolean result = flag; 
		flag = false; 
		return result; 
	}

	public static Set getmovees() {
		Set result = movees;
		movees = new HashSet();
		return result; 
	}
	
	pointcut moves():
		call(void Line.setP1(Point)) ||
		call(void Line.setP2(Point));
	
	after(): moves() {
		System.out.println(flag);
		flag = !flag;
	}
	
	pointcut moves2(Figure f, Point p):
		target(f) && args(p) && (call(void Line.setP1(Point)) ||
		call(void Line.setP2(Point))) ;
	
	after(Figure f, Point p): moves2(f,p) {
		movees.add(f);
	}
}
