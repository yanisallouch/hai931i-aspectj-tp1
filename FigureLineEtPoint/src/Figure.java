public class Figure {
	public static void main(String[] args) {
		Point poo = new Point();
		poo.setX(1);
		poo.setY(11);
		System.out.println("P1 created && setted");
		System.out.println("P1: " + poo );

		Point poo2 = new Point();
		poo2.setX(2);
		poo2.setY(22);
		System.out.println("P2 created && setted");
		System.out.println("P2: " + poo2);

		Line loo = new Line();
		loo.setP1(poo);
		System.out.println("setted P1");
		loo.setP2(poo2);
		System.out.println("setted P2");
		
		System.out.println("Done");
	}
	
	public String toString() {
		return "Figure";
	}
}
